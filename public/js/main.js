$(document).ready(function() {
    $('#toggle-signup').click(function() {
        $('#signup-form').show();
        $('#firstname').focus();
    });
    
    $('#signup-page #league').change(function() {
        //console.log('here');
        var league_id = $(this).attr('value');
        var ppt = leagues[league_id];
        //updatePlayerList(ppt);
        
        $.get("signup/playerlist/" + ppt,
        function(data){
          $('#player_fields').html(data);
        });
    });
    
    
    
    function updatePlayerList(num)
    {
        var groups = $('#signup-page dl.tabs dd');
        console.log('num: ' + num);
        groups.each(function(key, elm) {
            if ( ! $(elm).hasClass('substitute')) {
                console.log(key);
                if (key >= num) {
                    $(elm).css('display', 'none');
                } else {
                    $(elm).css('display', 'block');
                }
            }
        });
    }
    
    // add the rule here
    $.validator.addMethod("valueNotEquals", function(value, element, arg){
        return arg != value;
    }, "Please select a value");
    
    $.validator.setDefaults({
        ignore: []
    });
    
    $('.signup-form').validate({
        rules: {
            league: {
                required: true,
                valueNotEquals: 0
            },
            teamname: {
                required: true,
                minlength: 2
            },
            barname: {
                required: true,
                minlength: 2
            },
            address: {
                required: true,
                minlength: 10
            }
        },
        highlight: function(label) {
            $(label).addClass('error');
        },
        success: function(label) {
            label.addClass('valid').closest('.control-group').addClass('success');
        }
    });
    
//    $('#signup-page ul.tabs-content li *').each(function() {
//        var id = $(this).attr('id');
//        if (id.indexOf("email") == -1) {
//            $(this).rules('add', {required: true, minlength: 2});
//        }
//        
//    });
    
});

