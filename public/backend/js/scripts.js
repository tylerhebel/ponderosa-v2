$(document).ready(function() {
    
    $('#delete_season').click(function() {
        if ( ! confirm('Are you sure you want to DELETE this season?')) {
            return false;
        }
    });
    
    $('#delete_league').click(function() {
        if ( ! confirm('Are you sure you want to DELETE this league?')) {
            return false;
        }
    });
    
    $('#delete_user').click(function() {
        if ( ! confirm('Are you sure you want to DELETE this user?')) {
            return false;
        }
    });
    
    $('#delete_download').click(function() {
        if ( ! confirm('Are you sure you want to DELETE this download?')) {
            return false;
        }
    });
    
    $('#delete_content').click(function() {
        if ( ! confirm('Are you sure you want to DELETE this content?')) {
            return false;
        }
    });
    
    $('#delete_signup').click(function() {
        if ( ! confirm('Are you sure you want to DELETE this signup?')) {
            return false;
        }
    });
    
    $('#fetch-standings').click(function(e) {
        e.preventDefault();
		
		var button = $(this);
		var name = $('#name').val();
		
		if (jQuery.trim(name) == "")
		{
			alert('Please enter a league name');
			return;
		}
		
		var url = $('#standings_url').val() + name;
		
		button.addClass('disabled').attr('disabled', 'disabled');
		
		$('#standings-content').html('Loading ...');
		$.get(url, function(data) {
			$('#standings-content').html(data);
			button.removeClass('disabled').removeAttr('disabled');
		});
    });
    
    $('#clear-standings').click(function(e) {
		e.preventDefault();
		
		$('#standings-content').html('');
	});
	
	$('#fetch-schedule').click(function(e) {
		e.preventDefault();
		
		var button = $(this);
		var name = $('#name').val();
		
		if (jQuery.trim(name) == "")
		{
			alert('Please enter a league name');
			return;
		}
		
		var url = $('#schedule_url').val() + name;
		
		button.addClass('disabled').attr('disabled', 'disabled');
		
		$('#schedule-content').html('Loading ...');
		$.get(url, function(data) {
			$('#schedule-content').html(data);
			button.removeClass('disabled').removeAttr('disabled');
		});
	});
	
	$('#clear-schedule').click(function(e) {
		e.preventDefault();
		
		$('#schedule-content').html('');
	});
    
    $('#print_signup').bind('click', function() {
        window.print();
    });
    
    //$("#content_page textarea").wysihtml5();
    //$(".league_admin_page #captains").wysihtml5();
    
});