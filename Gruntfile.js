/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: 'ponderosa',
    meta: {
      banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
        '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
        '<%= pkg.homepage ? "* " + pkg.homepage + "\n" : "" %>' +
        '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;*/'
    },
    lint: {
      files: ['grunt.js', 'lib/**/*.js', 'test/**/*.js']
    },
    qunit: {
      files: ['test/**/*.html']
    },
    concat: {
      dist: {
        //src: ['<banner:meta.banner>', '<file_strip_banner:lib/<%= pkg.name %>.js>'],
        //src: ['<banner:meta.banner>', 'public/js/foundation/*.js'],
        src: [
            'public/js/foundation/jquery.js',
            'public/js/foundation/jquery.cookie.js',
            'public/js/foundation/jquery.event.move.js',
            'public/js/foundation/jquery.event.swipe.js',
            'public/js/foundation/jquery.foundation.accordion.js',
            'public/js/foundation/jquery.foundation.alerts.js',
            'public/js/foundation/jquery.foundation.buttons.js',
            'public/js/foundation/jquery.foundation.clearing.js',
            'public/js/foundation/jquery.foundation.forms.js',
            'public/js/foundation/jquery.foundation.joyride.js',
            'public/js/foundation/jquery.foundation.magellan.js',
            'public/js/foundation/jquery.foundation.mediaQueryToggle.js',
            'public/js/foundation/jquery.foundation.navigation.js',
            'public/js/foundation/jquery.foundation.orbit.js',
            'public/js/foundation/jquery.foundation.reveal.js',
            'public/js/foundation/jquery.foundation.tabs.js',
            'public/js/foundation/jquery.foundation.tooltips.js',
            'public/js/foundation/jquery.foundation.topbar.js',
            //'public/js/foundation/jquery.offcanvas.js',
            'public/js/foundation/jquery.placeholder.js',
            'public/js/foundation/app.js',
            'public/js/vendor/jquery.validate.min.js',
            'public/js/main.js'
        ],
        dest: 'public/js/ponderosa.js'
      }
    },
    min: {
      dist: {
        src: ['<banner:meta.banner>', '<config:concat.dist.dest>'],
        dest: 'public/js/<%= pkg.name %>.min.js'
      }
    },
    // watch: {
    //   files: '<config:lint.files>',
    //   tasks: 'lint concat min qunit'
    // },
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: true,
        boss: true,
        eqnull: true,
        browser: true
      },
      globals: {
        jQuery: true
      }
    },
    uglify: {}
  });

    grunt.loadNpmTasks('grunt-contrib-concat');


    // Default task.
  grunt.registerTask('default', ['concat']);

};