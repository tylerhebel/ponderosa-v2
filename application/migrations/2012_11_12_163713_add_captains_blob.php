<?php

class Add_Captains_Blob {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('leagues', function($table) {
            $table->text('captains_blob');
        });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('leagues', function($table) {
            $table->drop_column('captains_blob');
        });
	}

}