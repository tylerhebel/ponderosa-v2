<?php

class Create_Leagues_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('leagues', function($table) {
			$table->increments('id');
			$table->text('name');
			$table->text('description');
			$table->boolean('active');
			$table->integer('season_id');
			$table->timestamps();
		});
        
        // seed data
        $league = new League(array(
            'name' => 'League Extreme',
            'description' => 'Three person combo mixed ultra league',
            'active' => true,
            'season_id' => Season::find(1)->id
        ));
        
        $league->save();
        
        $schedule = new Schedule(array(
            'content' => 'This is my schedule'
        ));
        
        $standing = new Standing(array(
            'content' => 'This is my standing'
        ));
        
        $league->schedule()->insert($schedule);
        $league->standing()->insert($standing);
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('leagues');
	}

}