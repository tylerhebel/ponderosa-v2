<?php

class Delete_Seed_Signups {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		$registrants = Registrant::where('fullname', 'like', 'seed%');
        $registrants->delete();
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}