<?php

class Make_Admins {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		$user = User::where('email', '=', 'tylerhebel@gmail.com')->first();
        $user->is_admin = true;
        $user->save();
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}