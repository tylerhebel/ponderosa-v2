<?php

class Create_Downloads {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('downloads', function($table) {
			$table->increments('id');
			$table->text('filename');
            $table->text('path');
			$table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('downloads');
	}

}