<?php

class Create_User_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table) {
            $table->increments('id');
            $table->string('email', 128);
            $table->string('firstname', 128);
            $table->string('lastname', 128);
            $table->string('password', 64);
            $table->timestamps();
        });

		$user = new User(array(
            'email' => 'tylerhebel@gmail.com',
			'firstname' => 'Tyler',
			'lastname' => 'Hebel',
			'password' => Hash::make('thebel123'),	
        ));
        
        $user->save();
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}