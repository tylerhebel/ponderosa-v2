<?php

class Add_Active_To_Content {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contents', function($table) {
            $table->boolean('active');
        });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contents', function($table) {
            $table->drop_column('active');
        });
	}

}