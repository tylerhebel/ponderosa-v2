<?php

class Add_Player_Signup_Type {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('registrants', function($table) {
            $table->text('type');
        });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('registrants', function($table) {
            $table->drop_column('type');
        });
	}

}