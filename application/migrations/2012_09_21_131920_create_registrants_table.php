<?php

class Create_Registrants_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('registrants', function($table) {
            $table->increments('id');
            $table->string('fullname', 128);
            $table->string('email', 128);
            $table->string('phone', 32);
            $table->text('address');
            $table->timestamps();
        });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('registrants');
	}

}