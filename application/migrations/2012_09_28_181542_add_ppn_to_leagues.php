<?php

class Add_Ppn_To_Leagues {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('leagues', function($table) {
            $table->integer('players_per_team');
        });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('leagues', function($table) {
            $table->drop_column('players_per_team');
        });
	}

}