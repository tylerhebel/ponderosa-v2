<?php

class Seasons_Add_Registration {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('seasons', function($table)
        {
            $table->boolean('registration_open');
        });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('seasons', function($table)
        {
            $table->drop_column('registration_open');
        });
	}

}