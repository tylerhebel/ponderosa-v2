<?php

class Create_Signups_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('signups', function($table) {
            $table->increments('id');
            $table->integer('league_id');
            $table->text('team_name');
            $table->text('bar_name');
            $table->timestamps();
        });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('signups');
	}

}