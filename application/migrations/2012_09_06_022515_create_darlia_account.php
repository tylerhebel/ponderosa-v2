<?php

class Create_Darlia_Account {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		$user = new User(array(
            'email' => 'ponderosadarts@yahoo.com',
			'firstname' => 'Darlia',
			'lastname' => 'Simmons',
			'password' => Hash::make('darlia1231'),	
        ));
        
        $user->save();
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}