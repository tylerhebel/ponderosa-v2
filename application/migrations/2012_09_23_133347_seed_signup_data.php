<?php

class Seed_Signup_Data {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
        // add 'signup_id' to registrant table
        Schema::table('registrants', function($table) {
            $table->integer('signup_id');
        });
        
        // create signup
        $new_signup = array(
            'league_id' => League::first()->id,
            'team_name' => 'Seed Team',
            'bar_name' => 'Seed Bar',
        );
        
        $signup = new Signup($new_signup);
        $signup->save();
        
        // create registrants
		for ($i = 0; $i < 4; $i++)
        {
            $new_registrant = array(
                'signup_id' => $signup->id,
                'fullname' => "Seed User $i",
                'email' => "seed$i@domain.com",
                'phone' => '123-456-1234',
                'address' => "123 Fake St.\nBelvidere, IL\n61008",
            );
            
            $registrant = new Registrant($new_registrant);
            $registrant->save();
        }
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}