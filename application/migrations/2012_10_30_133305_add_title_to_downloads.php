<?php

class Add_Title_To_Downloads {

	public function up()
	{
		Schema::table('downloads', function($table) {
            $table->text('title');
        });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('downloads', function($table) {
            $table->drop_column('title');
        });
	}

}