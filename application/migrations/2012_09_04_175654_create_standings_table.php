<?php

class Create_Standings_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('standings', function($table) {
			$table->increments('id');
			$table->text('content');
			$table->integer('league_id');
			$table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('standings');
	}

}