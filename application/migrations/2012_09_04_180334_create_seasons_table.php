<?php

class Create_Seasons_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('seasons', function($table) {
            $table->increments('id');
            $table->text('name');
            $table->boolean('active');
            $table->timestamps();
        });
        
        // seed data
        $season = new Season(array(
           'name' => 'Summer 2012',
           'active' => true
        ));
        
        $season->save();
        
        $season = new Season(array(
           'name' => 'Fall 2012',
           'active' => true
        ));
        
        $season->save();
        
    }
    
	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('seasons');
	}

}