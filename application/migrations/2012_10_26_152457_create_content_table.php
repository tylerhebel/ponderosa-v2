<?php

class Create_Content_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contents', function($table) {
            $table->increments('id');
            $table->string('title', 128);
            $table->text('body');
            $table->string('user_id');
            $table->timestamps();
        });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contents');
	}

}