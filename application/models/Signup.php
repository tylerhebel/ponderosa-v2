<?php

class Signup extends Eloquent
{
    //public $includes = array('registrant');
    
    public function registrants()
    {
        return $this->has_many('Registrant');
    }
    
    public function league()
    {
        return $this->belongs_to('League');
    }
}

?>