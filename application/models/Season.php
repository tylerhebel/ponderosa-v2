<?php

class Season extends Eloquent 
{
    public static function getDropList()
    {
        $seasons = array();
        
        foreach (Season::all() as $season)
        {
            $seasons[$season->id] = $season->name;
        }
        
        return $seasons;
    }
    
    public static function getLeagueSignupList()
    {
        $seasons = array();
        
        $open_seasons = Season::where('registration_open', '=', 1)->order_by('name', 'asc')->get();
        foreach ($open_seasons as $season)
        {
            $these_leagues = League::where('season_id', '=', $season->id)->order_by('name', 'asc')->get();
            
            if (count($these_leagues) > 0)
            {
                $seasons[$season->name] = array();
            
                foreach ($these_leagues as $league)
                {
                    $seasons["$season->name"][$league->id] = $league->name . ' - ' . $league->description;
                }
            }
        }
        //var_dump($seasons);die();
        return $seasons;
    }
}

?>