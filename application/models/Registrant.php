<?php

class Registrant extends Eloquent
{
    public function signup()
    {
        return $this->belongs_to('Signup');
    }
}