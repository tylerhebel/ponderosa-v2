<?php

class League extends Eloquent
{
    public function schedule()
    {
        return $this->has_one('Schedule');
    }
    
    public function standing()
    {
        return $this->has_one('Standing');
    }
    
    public function signup()
    {
        return $this->has_many('Signup');
    }
    
    public static function getPlayersPerTeamSizes()
    {
        return array(
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 4
        );
    }
}

?>
