<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/

Route::get('/', function()
{
	return View::make('frontend.index');
});

Route::get('download', function() {
    $downloads = Download::all();
    return View::make('frontend.download')->with('downloads', $downloads);
});

Route::get('download/(:num)', function($download_id) {
    $download = Download::find($download_id);
    
    if ($download)
    {
        $path = $download->path;
        $file = $download->filename;
        
        return Response::download("$path/$file");
        //return Redirect::to('download');
    }
    else
    {
        return Response::error('404');
    }
    
});

Route::get('signup', array('before' => 'auth', function()
{
    $leagues = Season::getLeagueSignupList();
    
    if (count($leagues) < 1)
    {
        return View::make('frontend.emptysignup');
    }
    
    // prepend empty item entry
    array_unshift($leagues, '');
    
    $all_leagues = League::all();
    return View::make('frontend.signup')->with('leagues', $leagues)->with('all_leagues', $all_leagues);
}));

Route::get('signup/playerlist/(:num)', function($num_players) {
    return View::make('frontend.signup_playerlist')->with('num_players', $num_players);
});

Route::get('signup/success', function() {
    return View::make('frontend.signupsuccess');
});

Route::post('signup', function() {
    
//    $post = Input::all();
//    var_dump($post);
//    die();
    
    $new_signup = array(
        'team_name' => Input::get('teamname'),
        'bar_name' => Input::get('barname'),
    );
    
    if (Input::get('league') > 0)
    {
        $new_signup['league_id'] = Input::get('league');
    }
    
    $signup = new Signup($new_signup);
    $signup->save();
    
    // create registrants
    $players = Input::get('player');
    
    for ($i = 0; $i < count($players); $i++)
    {
        $new_registrant = array(
            'signup_id' => $signup->id,
            'fullname' => $players[$i]['fullname'],
            'email' => $players[$i]['email'],
            'phone' => $players[$i]['phone'],
            'address' => $players[$i]['address'],
            'type' => 'regular',
        );
        
        $registrant = new Registrant($new_registrant);
        $registrant->save();
    }
    
    // create subs
    $subs = Input::get('sub');
    for ($i = 0; $i < count($subs); $i++)
    {
        $new_sub = array(
            'signup_id' => $signup->id,
            'fullname' => $subs[$i]['fullname'],
            'email' => $subs[$i]['email'],
            'phone' => $subs[$i]['phone'],
            'address' => $subs[$i]['address'],
            'type' => 'regular',
        );
        
        if ($new_sub['fullname'] != '') 
        {
            $sub = new Registrant($new_sub);
            $sub->save();
        }
    }
    
    return Redirect::to('signup/success');
});

Route::get('leagues', function() {
    $leagues = League::where('active', '=', 1)->order_by('name', 'asc')->get();
    return View::make('frontend.leagues.list')->with('leagues', $leagues);
});

Route::get('leagues/(:num)', function($league_id) {
    $league = League::find($league_id);
    
    if ($league)
    {
        return View::make('frontend.leagues.view')->with('league', $league);
    }
    else
    {
        return Response::error('404');
    }
});

Route::get('news', function() {
    //$posts = Post::all();
    //return View::make('frontend.leagues')->with('leagues', $posts);
    return View::make('frontend.news');
});

Route::get('login', function() {
    return View::make('frontend.login');
});

Route::get('login/create', function() {
    return View::make('frontend.logincreate');
});

Route::get('logout', function() {
    Auth::logout();
    return Redirect::to('/');
});

Route::post('login', function()
{
    $credentials = array(
        'username' => Input::get('email'),
        'password' => Input::get('password'),
    );
    
    if (Auth::attempt($credentials))
    {
        if (Input::has('destination_url'))
        {
            return Redirect::to(Input::get('destination_url'));
        }
        
        return Redirect::to('user/profile');
    }
    
    return View::make('frontend.login')->with('flash', 'Invalid email or password');
});

Route::post('login/create', function() {
    
    $new_user = array(
        'firstname' => Input::get('firstname'),
        'lastname' => Input::get('lastname'),
        'email' => Input::get('email'),
        'password' => Input::get('password'),
        'password_confirmation' => Input::get('password_confirmation'),
        'is_admin' => 0
    );

    $rules = array(
        'firstname' => 'required',
        'lastname' => 'required',
        'email' => 'required|unique:users|email',
        'password' => 'required|confirmed',
    );

    $v = Validator::make($new_user, $rules);

    if ($v->fails())
    {
        return Redirect::to("login/create")
            ->with_errors($v)
            ->with_input();
    }

    $new_user['password'] = Hash::make($new_user['password']);

    unset($new_user['password_confirmation']);

    $user = new User($new_user);
    $user->save();
    
    Auth::login($user);

    return Redirect::to('user/profile')->with('message', "User Created.");
});

Route::get('user/profile', function() {
    return View::make('frontend.user.profile');
});


/*
|--------------------------------------------------------------------------
| Admin paths
|--------------------------------------------------------------------------
|
*/

Route::group(array('before' => 'auth'), function() {
    
    Route::get('admin', function() {
        return View::make('backend.index');
    });
    
    /*
     * LEAGUES
     */
    Route::get('admin/league', function() {
        $leagues = League::order_by('name', 'asc')->get();
        return View::make('backend.league.list')->with('leagues', $leagues);
    });
    Route::get('admin/league/create', function() {
        $seasons = Season::getDropList();
        return View::make('backend.league.form')->with('seasons', $seasons);
    });
    Route::post('admin/league/create', function() {
        
        $new_league = array(
            'name' => Input::get('name'),
            'description' => Input::get('description'),
            'captains_blob' => Input::get('captains'),
            'players_per_team' => Input::get('players_per_team'),
            'active' => is_null(Input::get('active')) ? 0 : 1,
            'season_id' => Input::get('season_id'),
        );
        
        $new_standing = array(
            'content' => Input::get('standings-content'),
        );
        
        $new_schedule = array(
            'content' => Input::get('schedule-content'),
        );
        
        $rules = array(
            'name' => 'required'
        );
        
        $v = Validator::make($new_league, $rules);
        
        if ($v->fails())
        {
            return Redirect::to('admin/league/create')
                ->with_errors($v)
                ->with_input();
        }
        
        $league = new League($new_league);
        $league->save();
        
        $standing = new Standing($new_standing);
        $league->standing()->insert($standing);
        
        $schedule = new Schedule($new_schedule);
        $league->schedule()->insert($schedule);
        
        return Redirect::to('admin/league')->with('message', "League $league->name Created!");
    });
    Route::get('admin/league/(:num)', function($league_id) {
        $league = League::find($league_id);
        return View::make('backend.league.form')
            ->with('league', $league)
            ->with('seasons', Season::getDropList());
    });
    Route::post('admin/league/(:num)', function($league_id) {
        $league = League::find($league_id);
        
        $league->name = Input::get('name');
        $league->description = Input::get('description');
        $league->captains_blob = Input::get('captains');
        $league->players_per_team = Input::get('players_per_team');
        $league->active = is_null(Input::get('active')) ? 0 : 1;
        $league->season_id = Input::get('season_id');
        
        $rules = array(
            'name' => 'required'
        );
        //var_dump($league);die();
        $v = Validator::make($league->to_array(), $rules);
        
        if ($v->fails())
        {
            return Redirect::to("admin/league/$league_id")
                ->with_errors($v)
                ->with_input();
        }
        
        $league->save();
        
        $standing = $league->standing;
        $standing->content = Input::get('standings-content');
        $standing->save();
        
        $schedule = $league->schedule;
        $schedule->content = Input::get('schedule-content');
        $schedule->save();
        
        return Redirect::to("admin/league/$league->id")->with('message', "League $league->name Updated!");
    });
    Route::get('admin/league/delete/(:num)', function($league_id) {
        $league = League::find($league_id);
        
        // delete from standings, schedules, and signups
        $standings = Standing::where('league_id', '=', $league->id);
        $standings->delete();
        
        $schedules = Schedule::where('league_id', '=', $league->id);
        $schedules->delete();
        
        $signups = Signup::where('league_id', '=', $league->id);
        $signups->delete();
        
        $message = "League $league->name Deleted!";
        
        $league->delete();
        
        return Redirect::to('admin/league')->with('message', $message);
    });
    
    /*
     * SEASONS
     */
    Route::get('admin/season', function() {
        $seasons = Season::all();
        return View::make('backend.season.list')
            ->with('seasons', $seasons);
    });
    Route::get('admin/season/create', function() {
        return View::make('backend.season.form');
    });
    Route::post('admin/season/create', function() {
        $new_season = array(
            'name' => Input::get('name'),
            'active' => is_null(Input::get('active')) ? 0 : 1,
            'registration_open' => is_null(Input::get('registration_open')) ? 0 : 1,
        );
        
        $rules = array(
            'name' => 'required|unique:seasons',
        );
        
        $v = Validator::make($new_season, $rules);
        
        if ($v->fails())
        {
            return Redirect::to('admin/season/create')
                ->with_errors($v)
                ->with_input();
        }
        
        $season = new Season($new_season);
        $season->save();
        
        return Redirect::to('admin/season')->with('message', "$season->name Created.");
    });
    
    Route::get('admin/season/(:num)', function($season_id) {
        $season = Season::find($season_id);
        return View::make('backend.season.form')->with('season', $season);
    });
    Route::post('admin/season/(:num)', function($season_id) {
        $season = Season::find($season_id);
        
        $season->name = Input::get('name');
        $season->active = is_null(Input::get('active')) ? 0 : 1;
        $season->registration_open = is_null(Input::get('registration_open')) ? 0 : 1;
        
        $rules = array(
            'name' => 'required',
        );
        
        $v = Validator::make($season->to_array(), $rules);
        
        if ($v->fails())
        {
            return Redirect::to("admin/season/$season_id")
                ->with_errors($v)
                ->with_input();
        }
        
        $season->save();
        
        return Redirect::to('admin/season')->with('message', "$season->name Updated!");
    });
    Route::get('admin/season/delete/(:num)', function($season_id) {
        $season = Season::find($season_id);
        
        $message = "$season->name Deleted!";
        
        $season->delete();
        
        return Redirect::to('admin/season')->with('message', $message);
    });
    
    /*
     * USERS
     */
    Route::get('admin/user', function() {
        $users = User::all();
        return View::make('backend.user.list')->with('users', $users);
    });
    Route::get('admin/user/create', function() {
        return View::make('backend.user.form');
    });
    Route::post('admin/user/create', function() {
        
        $new_user = array(
            'firstname' => Input::get('firstname'),
            'lastname' => Input::get('lastname'),
            'email' => Input::get('email'),
            'is_admin' => is_null(Input::get('is_admin')) ? 0 : 1,
            'password' => Input::get('password'),
            'password_confirmation' => Input::get('password_confirmation'),
        );
        
        $rules = array(
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|unique:users|email',
            'password' => 'confirmed',
        );
        
        $v = Validator::make($new_user, $rules);
        
        if ($v->fails())
        {
            return Redirect::to("admin/user/create")
                ->with_errors($v)
                ->with_input();
        }
        
        $new_user['password'] = Hash::make($new_user['password']);
        
        unset($new_user['password_confirmation']);
        
        $user = new User($new_user);
        $user->save();
        
        return Redirect::to('admin/user')->with('message', "User Created.");
    });
    Route::get('admin/user/(:num)', function($user_id) {
        $user = User::find($user_id);
        return View::make('backend.user.form')->with('user', $user);
    });
    Route::post('admin/user/(:num)', function($user_id) {
        $user = User::find($user_id);
        
        $user->firstname = Input::get('firstname');
        $user->lastname = Input::get('lastname');
        $user->email = Input::get('email');
        $user->is_admin = is_null(Input::get('is_admin')) ? 0 : 1;
        
        $rules = array(
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
        );
        
        $v = Validator::make($user->to_array(), $rules);
        
        if ($v->fails())
        {
            return Redirect::to("admin/user/$user_id")
                ->with_errors($v)
                ->with_input();
        }
        
        $user->save();
        
        return Redirect::to('admin/user')->with('message', "$user->firstname $user->lastname Updated!");
    });
    Route::get('admin/user/delete/(:num)', function($user_id) {
        $user = User::find($user_id);
        
        $message = "$user->firstname $user->lastname Deleted!";
        
        $user->delete();
        
        return Redirect::to('admin/user')->with('message', $message);
    });
    
    /*
     * SIGNUPS
     */
    
    Route::get('admin/signup', function() {
        $signups = Signup::order_by('created_at', 'desc')->get();
        //var_dump($signups);
        //die();
        return View::make('backend.signup.list')->with('signups', $signups);
    });
    Route::get('admin/signup/filter/(:num)', function($league_id) {
        $signups = Signup::where('league_id', '=', $league_id)->get();
        return View::make('backend.signup.list')->with('signups', $signups);
    });
    Route::get('admin/signup/create', function() {
        return View::make('backend.signup.form');
    });
    Route::post('admin/signup/create', function() {
        
    });
    Route::get('admin/signup/(:num)', function($signup_id) {
        $signup = Signup::find($signup_id);
        return View::make('backend.signup.view')->with('signup', $signup);
    });
    Route::post('admin/signup/(:num)', function($signup_id) {
        
    });
    Route::get('admin/signup/delete/(:num)', function($signup_id) {
        $signup = Signup::find($signup_id);
        
        $message = "Sign up deleted!";
        
        $registrants = Registrant::where('signup_id', '=', $signup->id);
        $registrants->delete();
        
        $signup->delete();
        
        return Redirect::to('admin/signup')->with('message', $message);
    });
    
    
    /*
     * Content
     */
    Route::get('admin/content', function() {
        $contents = Content::all();
        return View::make('backend.content.list')->with('contents', $contents);
    });
    
    Route::get('admin/content/create', function() {
        return View::make('backend.content.form');
    });
    Route::post('admin/content/create', function() {
        
        $new_content = array(
            'title' => Input::get('title'),
            'body' => Input::get('body'),
            'user_id' => Auth::user()->id,
            'active' => is_null(Input::get('active')) ? 0 : 1,
        );
        
        $rules = array(
            'title' => 'required',
            'body' => 'required',
        );
        
        $v = Validator::make($new_content, $rules);
        
        if ($v->fails())
        {
            return Redirect::to("admin/content/create")
                ->with_errors($v)
                ->with_input();
        }
        
        $content = new Content($new_content);
        $content->save();
        
        return Redirect::to('admin/content')->with('message', "Content Created.");
    });
    Route::get('admin/content/(:num)', function($content_id) {
        $content = Content::find($content_id);
        return View::make('backend.content.form')->with('content', $content);
    });
    Route::post('admin/content/(:num)', function($content_id) {
        $content = Content::find($content_id);
        
        $content->title = Input::get('title');
        $content->body = Input::get('body');
        $content->active = is_null(Input::get('active')) ? 0 : 1;
        
        $rules = array(
            'title' => 'required',
            'body' => 'required',
        );
        
        $v = Validator::make($content->to_array(), $rules);
        
        if ($v->fails())
        {
            return Redirect::to("admin/content/$content_id")
                ->with_errors($v)
                ->with_input();
        }
        
        $content->save();
        
        return Redirect::to('admin/content')->with('message', "$content->title Updated!");
    });
    Route::get('admin/content/delete/(:num)', function($content_id) {
        $content = Content::find($content_id);
        
        $message = "$content->title Deleted!";
        
        $content->delete();
        
        return Redirect::to('admin/content')->with('message', $message);
    });
    
    /*
     * DOWNLOADS
     */
    
    Route::get('admin/download', function() {
        $downloads = Download::all();
        return View::make('backend.download.list')->with('downloads', $downloads);
    });
    Route::get('admin/download/create', function() {
        return View::make('backend.download.form');
    });
    Route::post('admin/download/create', function() {
        $file = Input::file('file');
        
        $new_download = array(
            'title' => Input::get('title'),
            'filename' => $file['name'],
            'path' => 'storage/downloads',
            'file' => Input::file('file'),
        );
        
        $rules = array(
            'title' => 'required',
            'file' => 'required',
        );
        
        $v = Validator::make($new_download, $rules);
        
        if ($v->fails())
        {
            return Redirect::to("admin/download/create")
                ->with_errors($v)
                ->with_input();
        }
        
        unset($new_download['file']);
        
        $download = new Download($new_download);
        $download->save();
        
        Input::upload('file', 'storage/downloads', $file['name']);
        
        return Redirect::to('admin/download')->with('message', $file['name'] . 'has been uploaded.');
    });
    
    Route::get('admin/download/delete/(:num)', function($download_id) {
        $download = Download::find($download_id);
        
        $message = "download->title Deleted!";
        
        $download->delete();
        
        return Redirect::to('admin/download')->with('message', $message);
    });
    
});


Route::get('api/standings/(:all)', function($league) {
    echo FTP::fetch('standings', $league);
});

Route::get('api/schedule/(:all)', function($league) {
    echo FTP::fetch('schedule', $league);
});

/*
|--------------------------------------------------------------------------
| Application 404 & 500 Error Handlers
|--------------------------------------------------------------------------
|
| To centralize and simplify 404 handling, Laravel uses an awesome event
| system to retrieve the response. Feel free to modify this function to
| your tastes and the needs of your application.
|
| Similarly, we use an event to handle the display of 500 level errors
| within the application. These errors are fired when there is an
| uncaught exception thrown in the application.
|
*/

Event::listen('404', function()
{
	return Response::error('404');
});

Event::listen('500', function()
{
	return Response::error('500');
});

/*
|--------------------------------------------------------------------------
| Route Filters
|--------------------------------------------------------------------------
|
| Filters provide a convenient method for attaching functionality to your
| routes. The built-in before and after filters are called before and
| after every request to your application, and you may even create
| other filters that can be attached to individual routes.
|
| Let's walk through an example...
|
| First, define a filter:
|
|		Route::filter('filter', function()
|		{
|			return 'Filtered!';
|		});
|
| Next, attach the filter to a route:
|
|		Router::register('GET /', array('before' => 'filter', function()
|		{
|			return 'Hello World!';
|		}));
|
*/

Route::filter('before', function()
{
	// Do stuff before every request to your application...
});

Route::filter('after', function($response)
{
	// Do stuff after every request to your application...
});

Route::filter('csrf', function()
{
	if (Request::forged()) return Response::error('500');
});

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::to('login')->with('destination_url', URL::current());
});