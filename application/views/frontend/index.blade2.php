@layout('frontend/layout')

@section('content')

@foreach (Content::where('active', '=', 1)->order_by('created_at', 'desc')->get() as $content)


<div class="content_body">
    {{ $content->body }}
</div>
<div class="content_meta">
    <?php $date = new DateTime($content->created_at); $date = $date->format('F jS, Y @ g:ia'); ?>
    <b>{{ User::find($content->user_id)->firstname }}</b> posted at {{ $date }}
</div>

<hr/>

@endforeach

@endsection