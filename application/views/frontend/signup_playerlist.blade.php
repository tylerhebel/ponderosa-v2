<fieldset>
    <legend>Player Information</legend>
    <dl class="tabs">
        @for ($i = 0; $i < $num_players; $i++)
        <dd {{ ($i == 0) ? 'class="active"' : ''; }}><a href="#player{{ $i+1 }}" id="player{{ $i+1 }}tablink">Player {{ $i+1 }}</a></dd>
        @endfor
<!--                        <dd><a href="#player2">Player 2</a></dd>
        <dd><a href="#simple3">Player 3</a></dd>
        <dd><a href="#simple4">Player 4</a></dd>-->
        <dd class="substitute"><a href="#sub1">Sub 1</a></dd>
        <dd class="substitute"><a href="#sub2">Sub 2</a></dd>
    </dl>
    <br/><br/>

    <ul class="tabs-content">

        @for ($i = 0; $i < $num_players; $i++)
        <li {{ ($i == 0) ? 'class="active"' : ''; }} id="player{{ $i+1 }}Tab">
            <div class="row">
                <h3>Player {{ $i+1 }} {{ $i == 0 ? ' - (Team Captain)' : '' }}</h3>
                <div class="five columns">
                    {{ Form::label("player[$i][fullname]", 'Name') }}
                    {{ Form::text("player[$i][fullname]", '', array('class' => 'required')) }}

                    {{ Form::label("player[$i][email]", 'Email') }}
                    {{ Form::text("player[$i][email]") }}

                    {{ Form::label("player[$i][phone]", 'Cell') }}
                    {{ Form::text("player[$i][phone]", '', array('class' => 'required')) }}
                </div>
                <div class="six columns offset-by-one">
                    {{ Form::label("player[$i][address]", 'Address') }}
                    {{ Form::textarea("player[$i][address]", "", array('rows' => '4', 'class' => 'required')) }}        
                </div>
            </div>
        </li>
        @endfor

        <li id="sub1Tab">
            <div class="row">
                <h3>Substitute 1 <small class="alert-error">Optional</small></h3>
                <div class="five columns">
                    {{ Form::label("sub[0][fullname]", 'Name') }}
                    {{ Form::text("sub[0][fullname]") }}

                    {{ Form::label("sub[0][email]", 'Email') }}
                    {{ Form::text("sub[0][email]") }}

                    {{ Form::label("sub[0][phone]", 'Cell') }}
                    {{ Form::text("sub[0][phone]") }}
                </div>
                <div class="six columns offset-by-one">
                    {{ Form::label("sub[0][address]", 'Address') }}
                    {{ Form::textarea("sub[0][address]", "", array('rows' => '4')) }}        
                </div>
            </div>
        </li>

        <li id="sub2Tab">
            <div class="row">
                <h3>Substitute 2 <small>Optional</small></h3>
                <div class="five columns">
                    {{ Form::label("sub[1][fullname]", 'Name') }}
                    {{ Form::text("sub[1][fullname]") }}

                    {{ Form::label("sub[1][email]", 'Email') }}
                    {{ Form::text("sub[1][email]") }}

                    {{ Form::label("sub[1][phone]", 'Cell') }}
                    {{ Form::text("sub[1][phone]") }}
                </div>
                <div class="six columns offset-by-one">
                    {{ Form::label("sub[1][address]", 'Address') }}
                    {{ Form::textarea("sub[1][address]", "", array('rows' => '4')) }}        
                </div>
            </div>
        </li>

    </ul>
</fieldset>