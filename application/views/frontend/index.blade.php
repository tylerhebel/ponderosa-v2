@layout('frontend/layout')

@section('content')


<div class="row">
    <div class="twelve columns">
        <div class="row">
            <div class="six columns">
                
                <h2>Leagues</h2><hr/>
                
                @foreach (League::where('active', '=', 1)->order_by('name', 'asc')->get() as $league)
                <div class="row">
                    <div class="nine columns">
                        <h3>{{ HTML::link("leagues/$league->id", $league->name) }} <small>{{ $league->description }}</small></h3>
                    </div>
                </div><hr/>
                @endforeach
             
            </div>
            <div class="six mobile-four columns">

                <h2>News</h2><hr/>
                
                @foreach (Content::where('active', '=', 1)->order_by('created_at', 'desc')->get() as $content)
                
                <?php
                $date = new DateTime($content->created_at); 
                $date = $date->format('F jS, Y'); 
                ?>
                <div class="panel">
                    <h3>{{ $content->title }}</h3>

                    <p>{{ $content->body }}</p>
                    
                    <hr/>
                    <h6 class="subheader"><i>{{ User::find($content->user_id)->firstname }} posted on {{ $date }}</i></h6>
                </div>
                @endforeach
                
            </div>
        </div>
    </div>
</div>

<!-- End Content -->

@endsection