@layout('frontend.layout')

@section('content')

<h1>League Signup</h1>

<p>This is a TEAM signup sheet, get together with your team captain to fill this out.</p>

<div id="signup-page" class="row">
    <div class="twelve columns">
        {{ Form::open('signup', 'POST', array('class' => 'form signup-form')) }}

        <div class="row">
            <div class="three columns">
                <fieldset>
                    <legend>Team Information</legend>
                    {{ Form::label('league', 'League Name') }}
                    {{ Form::select('league', $leagues) }}
                    <br/><br/>

                    {{ Form::label('teamname', 'Team name') }}
                    {{ Form::text('teamname') }}

                    {{ Form::label('bar', 'Bar, Tavern, or Pub name') }}
                    {{ Form::text('barname') }}

                </fieldset>
            </div>

            <div class="nine columns" id="player_fields">
                <fieldset>
                    <legend>Player Information</legend>
                    <div class="alert-box">Please select a league.</div>
                </fieldset>
            </div>
        </div>
        
        <input type="submit" class="large button" value="Submit Registration" id="reg_submit"/>

        {{ Form::close() }}
    </div>
</div>

<script type="text/javascript">
    var leagues = {
        @foreach ($all_leagues as $league)
        {{ $league->id }}: "{{ $league->players_per_team }}",
        @endforeach
    }
</script>


@endsection