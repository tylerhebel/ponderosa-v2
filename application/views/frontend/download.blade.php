@layout('frontend.layout')

@section('content')

<h1>Downloads</h1>

<div class="grid">
    
    @foreach ($downloads as $download)
    
    <div class="grid-cell">
        <h3><span class="star">&#x2660;</span> {{ HTML::link("download/$download->id", $download->title) }}</h3>
    </div>
    
    @endforeach

</div>

@endsection
