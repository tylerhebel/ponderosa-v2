@layout('frontend.layout')

@section('content')

<h1>League Signup</h1>

<p>This is a TEAM signup sheet, get together with your team captain to fill this out.</p>

<div id="signup-page">
{{ Form::open('signup', 'POST', array('class' => 'form signup-form')) }}

    <div class="grid">
        <div class="grid-cell">
            <div class="control-group">
                {{ Form::label('league', 'Select the league you\'re signing up for') }}
                <div class="controls">
                    {{ Form::select('league', $leagues) }}
                </div>
            </div>
            
            <div class="control-group">
                {{ Form::label('teamname', 'Team name') }}
                <div class="controls">
                    {{ Form::text('teamname') }}
                </div>
            </div>
            
            <div class="control-group">
                {{ Form::label('bar', 'Bar, Tavern, or Pub name') }}
                <div class="controls">
                    {{ Form::text('barname' ) }}
                </div>
            </div>
            
        </div>
        <div class="grid-cell">
            
            <div class="accordion" id="player_accordion">
                
                @for ($i = 0; $i < 4; $i++)
                <div class="accordion-group regular_players" id="group{{ $i }}">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#player_accordion" href="#collapse{{ $i }}">
                            Player {{ $i+1 }} {{ $i == 0 ? ' - (Team Captain)' : '' }}
                        </a>
                    </div>
                    <div id="collapse{{ $i }}" class="accordion-body collapse {{ $i == 0 ? 'in' : '' }}">
                        <div class="accordion-inner">
                            <div class="control-group">
                                {{ Form::label("player[$i][fullname]", 'Name') }}
                                <div class="controls">
                                    {{ Form::text("player[$i][fullname]") }}
                                </div>
                            </div>
                            
                            <div class="control-group">
                                {{ Form::label("player[$i][email]", 'Email') }}
                                <div class="controls">
                                    {{ Form::text("player[$i][email]") }}
                                </div>
                            </div>
                            
                            <div class="control-group">
                                {{ Form::label("player[$i][phone]", 'Cell') }}
                                <div class="controls">
                                    {{ Form::text("player[$i][phone]") }}
                                </div>
                            </div>
                            
                            <div class="control-group">
                                {{ Form::label("player[$i][address]", 'Address') }}
                                <div class="controls">
                                    {{ Form::textarea("player[$i][address]", "", array('rows' => '4')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endfor
                
                @for ($i = 4; $i < 6; $i++)
                <div class="accordion-group substitute" id="group{{ $i }}">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#player_accordion" href="#collapse{{ $i }}">
                            Substitute {{ $i-3 }}
                        </a>
                    </div>
                    <div id="collapse{{ $i }}" class="accordion-body collapse {{ $i == 0 ? 'in' : '' }}">
                        <div class="accordion-inner">
                            <div class="control-group">
                                {{ Form::label("player[$i][fullname]", 'Name') }}
                                <div class="controls">
                                    {{ Form::text("player[$i][fullname]") }}
                                </div>
                            </div>
                            
                            <div class="control-group">
                                {{ Form::label("player[$i][email]", 'Email') }}
                                <div class="controls">
                                    {{ Form::text("player[$i][email]") }}
                                </div>
                            </div>
                            
                            <div class="control-group">
                                {{ Form::label("player[$i][phone]", 'Cell') }}
                                <div class="controls">
                                    {{ Form::text("player[$i][phone]") }}
                                </div>
                            </div>
                            
                            <div class="control-group">
                                {{ Form::label("player[$i][address]", 'Address') }}
                                <div class="controls">
                                    {{ Form::textarea("player[$i][address]", "", array('rows' => '4')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endfor
            </div>
        </div>
    </div>
    
    <br/><br/>
    
    {{ Form::submit('Signup!', array('class' => 'btn btn-success btn-large')) }}

{{ Form::close() }}
</div>

<script type="text/javascript">
    var leagues = {
        @foreach ($all_leagues as $league)
        {{ $league->id }}: "{{ $league->players_per_team }}",
        @endforeach
    }
</script>


@endsection