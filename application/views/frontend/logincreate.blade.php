@layout('frontend.layout')

@section('content')

<h1 class="subheader">New Players</h1>

<p>Use this form to create a Ponderosa player account.</p>

{{ Form::open('login/create', 'POST', array('class' => 'form pull-left')) }}

    <div class="control-group <?php echo ($errors->has('firstname')) ? 'error' : ''; ?>">
        {{ Form::label('firstname', 'First Name', array('class' => 'control-label')) }}

        <div class="controls">
            {{ Form::text('firstname', Input::old('firstname')) }}
            @if ($errors->has('firstname'))
            <span class="help-inline">{{ $errors->first('firstname') }}</span>
            @endif
        </div>
    </div>
    <div class="control-group <?php echo ($errors->has('lastname')) ? 'error' : ''; ?>">
        {{ Form::label('lastname', 'Last Name', array('class' => 'control-label')) }}

        <div class="controls">
            {{ Form::text('lastname', Input::old('lastname')) }}
            @if ($errors->has('lastname'))
            <span class="help-inline">{{ $errors->first('lastname') }}</span>
            @endif
        </div>
    </div>
    <div class="control-group <?php echo ($errors->has('email')) ? 'error' : ''; ?>">
        {{ Form::label('email', 'Email Address', array('class' => 'control-label')) }}

        <div class="controls">
            {{ Form::text('email', Input::old('email')) }}
            @if ($errors->has('email'))
            <span class="help-inline">{{ $errors->first('email') }}</span>
            @endif
        </div>
    </div>

    <div class="control-group <?php echo ($errors->has('password')) ? 'error' : ''; ?>">
        {{ Form::label('password', 'Password', array('class' => 'control-label')) }}

        <div class="controls">
            {{ Form::password('password') }}
            @if ($errors->has('password'))
            <span class="help-inline">{{ $errors->first('password') }}</span>
            @endif
        </div>
    </div>
    <div class="control-group <?php echo ($errors->has('password')) ? 'error' : ''; ?>">
        {{ Form::label('password_confirmation', 'Confirm Password', array('class' => 'control-label')) }}

        <div class="controls">
            {{ Form::password('password_confirmation') }}
            @if ($errors->has('confirm_password'))
            <span class="help-inline">{{ $errors->first('confirm_password') }}</span>
            @endif
        </div>
    </div>


    {{ Form::submit('Create', array('class' => 'button')) }}

{{ Form::close() }}
    

@endsection