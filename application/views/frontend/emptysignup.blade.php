@layout('frontend.layout')

@section('content')

<h1>Sorry, no leagues are currently accepting signups</h1>

<p><strong>Thanks for interest!</strong></p>
<p>Please check back soon or check out the contact information below.</p>

@endsection