<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
   
    
@include('partials/header')
    
<body>


@include('partials/nav')

@if (URI::is('/'))
@include('partials/promo')
@endif


<div class="row">
    <div class="twelve columns">
        @yield('content')
    </div>
</div>

@include('partials/footer')

<!-- Included JS Files -->
<?php echo HTML::script('js/ponderosa.min.js'); ?>

</body>

</html>

