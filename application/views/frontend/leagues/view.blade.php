@layout('frontend.layout')

@section('content')

<h1><a name="#top">{{ $league->name }} - {{ $league->description }}</a></h1>

<dl class="sub-nav">
  <dt>Jump To:</dt>
  <dd class="active"><a href="#standings">Standings</a></dd>
  <dd class="active"><a href="#schedule">Schedule</a></dd>
</dl>

@if ( ! empty($league->captains_blob))
<h2><a name="#captains">Team Captains</a></h2>
{{ $league->captains_blob }}
@endif


<h2><a name="#standings">Standings</a></h2>
<dl class="sub-nav">
  <dd class="active"><a href="#top">Back to top</a></dd>
</dl>
<pre>{{ $league->standing->content }}</pre>


<h2><a name="schedule">Schedule</a></h2>
<dl class="sub-nav">
  <dd class="active"><a href="#top">Back to top</a></dd>
</dl>
<pre>{{ $league->schedule->content }}</pre>

@endsection