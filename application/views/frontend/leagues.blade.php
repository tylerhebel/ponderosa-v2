@layout('frontend.layout')

@section('content')

<h1>Leagues</h1>

<div class="grid">
    
    @foreach ($leagues as $league)
    
    <div class="grid-cell">
        <h3><span class="star">&#x2660;</span> {{ HTML::link("leagues/$league->id", $league->name) }}</h3>
        <p>{{ $league->description }}</p>
    </div>
    
    @endforeach
</div>

@endsection