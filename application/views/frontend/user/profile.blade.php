@layout('frontend.layout')

@section('content')

<h1>Account Profile</h1>

<div class="address" style="margin-top: 20px;">
    <strong>First Name</strong>
    <p>{{ Auth::user()->firstname }}</p>
    
    <strong>Last Name</strong>
    <p>{{ Auth::user()->lastname }}</p>
    
    <strong>Email</strong>
    <p>{{ Auth::user()->email }}</p>
</div>

@endsection