@layout('frontend.layout')

@section('content')

<h1>Login</h1>

<div class="row">
    <div class="twelve columns" id="existing_login">
        
        <div class="row">
            <div class="six columns">
                <h2 class="subheader">Returning Players</h2>

                {{ Form::open('login', 'POST', array('class' => 'form pull-left')) }}

                    @if (isset($flash))
                    <div class="alert alert-error">{{ $flash }}</div>
                    @endif

                    @if (Session::has('destination_url'))
                    {{ Form::hidden('destination_url', Session::get('destination_url')) }}
                    @endif

                    <div class="control-group <?php echo ($errors->has('email')) ? 'error' : ''; ?>">
                        {{ Form::label('email', 'Email Address', array('class' => 'control-label')) }}

                        <div class="controls">
                            {{ Form::text('email', Input::old('email')) }}
                            @if ($errors->has('email'))
                            <span class="help-inline">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="control-group <?php echo ($errors->has('password')) ? 'error' : ''; ?>">
                        {{ Form::label('password', 'Password', array('class' => 'control-label')) }}

                        <div class="controls">
                            {{ Form::password('password') }}
                            @if ($errors->has('password'))
                            <span class="help-inline">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                    </div>


                    {{ Form::submit('Login', array('class' => 'large button radius')) }}

                {{ Form::close() }}
            </div>

            <div class="six columns" id="create_login">
                <h2 class="subheader">New Players</h2>
                <p>If you don't have a Ponderosa player account, {{ HTML::link('login/create', 'Click Here') }} to create one.</p>

            </div>
        </div>
    </div>
</div>

@endsection