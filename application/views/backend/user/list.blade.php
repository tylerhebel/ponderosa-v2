@layout('backend.layout')

@section('content')

<div class="span12">
    <h1>User List</h1>
    
    @if (Session::has('message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Success!</strong> {{ Session::get('message') }}
    </div>
    @endif
    
    {{ HTML::link('admin/user/create', 'Create New User', array('class' => 'btn')) }}
    <br/><br/>
    <table class="table table-hover table-bordered table-striped">
        <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Is Admin</th>
                <th>Created On</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
            <tr {{ $user->is_admin == 1 ? 'class="info"' : '' }}>
                <td>{{ $user->firstname }}</td>
                <td>{{ $user->lastname }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ Yesno::write($user->is_admin) }}</td>
                <td>{{ $user->created_at }}</td>
                <td>{{ HTML::link("admin/user/" . $user->id, 'Edit', array('class' => 'btn btn-warning')) }}</td>
                
            </tr>
            @endforeach
        </tbody>
    </table>
</div>



@endsection