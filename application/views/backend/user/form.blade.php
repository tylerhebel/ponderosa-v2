@layout('backend.layout')

@section('content')

<?php

$create = isset($user) ? false : true; 

?>

<div class="span12">
    @if ($create)
    <h1>Create New User</h1>
    @else
    <h1>Edit User <small>{{ $user->firstname . ' ' . $user->lastname }}</small></h1>
    @endif
    
    {{ Form::open('admin/user/' . ($create ? 'create' : $user->id), 'POST', array('class' => 'form-horizontal well')) }}

    <div class="control-group <?php echo ($errors->has('firstname')) ? 'error' : ''; ?>">
        {{ Form::label('firstname', 'First Name', array('class' => 'control-label')) }}

        <div class="controls">
            {{ Form::text('firstname', Input::old('firstname') || $create ? Input::old('firstname') : $user->firstname, array('class' => 'input-xlarge')) }}
            @if ($errors->has('firstname'))
            <span class="help-inline">{{ $errors->first('firstname') }}</span>
            @endif
        </div>
    </div>
    <div class="control-group <?php echo ($errors->has('lastname')) ? 'error' : ''; ?>">
        {{ Form::label('lastname', 'Last Name', array('class' => 'control-label')) }}

        <div class="controls">
            {{ Form::text('lastname', Input::old('lastname') || $create ? Input::old('lastname') : $user->lastname, array('class' => 'input-xlarge')) }}
            @if ($errors->has('lastname'))
            <span class="help-inline">{{ $errors->first('lastname') }}</span>
            @endif
        </div>
    </div>
    <div class="control-group <?php echo ($errors->has('email')) ? 'error' : ''; ?>">
        {{ Form::label('email', 'Email Address', array('class' => 'control-label')) }}

        <div class="controls">
            {{ Form::text('email', Input::old('email') || $create ? Input::old('email') : $user->email, array('class' => 'input-xlarge')) }}
            @if ($errors->has('email'))
            <span class="help-inline">{{ $errors->first('email') }}</span>
            @endif
        </div>
    </div>
    
    @if ($create)
    <div class="control-group <?php echo ($errors->has('password')) ? 'error' : ''; ?>">
        {{ Form::label('password', 'Password', array('class' => 'control-label')) }}

        <div class="controls">
            {{ Form::text('password', '', array('class' => 'input-xlarge')) }}
            @if ($errors->has('password'))
            <span class="help-inline">{{ $errors->first('password') }}</span>
            @endif
        </div>
    </div>
    <div class="control-group <?php echo ($errors->has('password_confirmation')) ? 'error' : ''; ?>">
        {{ Form::label('password_confirmation', 'Confirm Password', array('class' => 'control-label')) }}

        <div class="controls">
            {{ Form::text('password_confirmation', '', array('class' => 'input-xlarge')) }}
            @if ($errors->has('confirm_password'))
            <span class="help-inline">{{ $errors->first('confirm_password') }}</span>
            @endif
        </div>
    </div>
    @endif
    
    <div class="control-group">
        {{ Form::label('admin', 'Is Admin?', array('class' => 'control-label')) }}

        <div class="controls">
            <label class="checkbox">{{ Form::checkbox('is_admin', 'is_admin', isset($user->is_admin) && $user->is_admin == 1 ? 1 : 0) }}</label>
            <span class="help-block"><span class="label">Admin</span> users get access to the admin.  People like you Darlia and Todd and Tyler.</span>
        </div>
    </div>
    <div class="form-actions">
        {{ Form::submit('Save', array('class' => 'btn btn-success')) }}
        {{ HTML::link('admin/user', 'Cancel & Go Back', array('class' => 'btn')) }}
        
        @if (!$create)
        {{ HTML::link("admin/user/delete/$user->id", 'Delete', array('id' => 'delete_user', 'class' => 'btn btn-danger pull-right')) }}
        @endif
    </div>
    {{ Form::close() }}
</div>

@endsection