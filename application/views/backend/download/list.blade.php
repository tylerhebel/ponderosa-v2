@layout('backend.layout')

@section('content')

<div class="span12">
    <h1>Downloads</h1>
    
    @if (Session::has('message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Success!</strong> {{ Session::get('message') }}
    </div>
    @endif
    
    {{ HTML::link('admin/download/create', 'Create New Download', array('class' => 'btn')) }}
    <br/><br/>
    <table class="table table-hover table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Filename</th>
                <th>Filepath</th>
                <th>Content</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 1; ?>
            @foreach ($downloads as $download)
            <tr>
                <td>{{ $i }}</td>
                <td>{{ $download->title }}</td>
                <td>{{ $download->filename }}</td>
                <td>{{ $download->path }}</td>
                <td>{{ date('m-d-Y H:i', strtotime($download->created_at)) }}</td>
                <td>{{ HTML::link("admin/download/delete/$download->id", 'Delete', array('id' => 'delete_download', 'class' => 'btn btn-danger')) }}</td>
            </tr>
            
            <?php $i++; ?>
            @endforeach
        </tbody>
    </table>
</div>



@endsection