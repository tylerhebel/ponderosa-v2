@layout('backend.layout')

@section('content')

<?php

$create = isset($download) ? false : true; 

?>

<div class="span12">
    @if ($create)
    <h1>Create New Download</h1>
    @else
    <h1>Edit Download <small>{{ $download->title }}</small></h1>
    @endif
    
    {{ Form::open_for_files('admin/download/' . ($create ? 'create' : $download->id), 'POST', array('class' => 'form-horizontal well')) }}

    <div class="control-group <?php echo ($errors->has('title')) ? 'error' : ''; ?>">
        {{ Form::label('title', 'Title', array('class' => 'control-label')) }}

        <div class="controls">
            {{ Form::text('title', Input::old('title') || $create ? Input::old('title') : $download->title, array('class' => 'input-xlarge')) }}
            @if ($errors->has('title'))
            <span class="help-inline">{{ $errors->first('title') }}</span>
            @endif
        </div>
    </div>
    <div class="control-group <?php echo ($errors->has('title')) ? 'error' : ''; ?>">
        {{ Form::label('file', 'File', array('class' => 'control-label')) }}

        <div class="controls">
            {{ Form::file('file', array('class' => 'input-xlarge')) }}
            @if ($errors->has('file'))
            <span class="help-inline">{{ $errors->first('file') }}</span>
            @endif
        </div>
    </div>
    <div class="form-actions">
        {{ Form::submit('Save', array('class' => 'btn btn-success')) }}
        {{ HTML::link('admin/download', 'Cancel & Go Back', array('class' => 'btn')) }}
        
        @if (!$create)
        {{ HTML::link("admin/download/delete/$download->id", 'Delete', array('id' => 'delete_download', 'class' => 'btn btn-danger pull-right')) }}
        @endif
    </div>
    {{ Form::close() }}
</div>

@endsection