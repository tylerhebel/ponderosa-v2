<div class="well" style="padding: 8px 0;">
	<ul class="nav nav-list">
		
		<li class="nav-header">League Management</li>
		
		<li class="{{ URI::segment(2) == 'league' ? 'active' : ''}}"><a href="{{ URL::to('admin/league') }}"><i class="icon-fire"></i> Leagues</a></li>
		<li class="{{ URI::segment(2) == 'season' ? 'active' : ''}}"><a href="{{ URL::to('admin/season') }}"><i class="icon-fire"></i> Seasons</a></li>
		<!-- 
		<li class="{{ URI::segment(2) == 'standings' ? 'active' : ''}}"><a href="{{ URL::to('admin/standings') }}"><i class="icon-leaf"></i> Standings</a></li>
		<li class="{{ URI::segment(2) == 'schedule' ? 'active' : ''}}"><a href="{{ URL::to('admin/schedule') }}"><i class="icon-certificate"></i> Schedules</a></li>
		-->
		<li class="divider"></li>
		
		<li class="nav-header">User Management</li>
		<li class="{{ URI::segment(2) == 'user' ? 'active' : ''}}"><a href="{{ URL::to('admin/user') }}"><i class="icon-user"></i> Users</a></li>
        <li class="{{ URI::segment(2) == 'signup' ? 'active' : ''}}"><a href="{{ URL::to('admin/signup') }}"><i class="icon-pencil"></i> Signups</a></li>
        <li class="{{ URI::segment(2) == 'content' ? 'active' : ''}}"><a href="{{ URL::to('admin/content') }}"><i class="icon-book"></i> Content</a></li>
        <li class="{{ URI::segment(2) == 'download' ? 'active' : ''}}"><a href="{{ URL::to('admin/download') }}"><i class="icon-download"></i> Download</a></li>
		
<!--		<li class="nav-header">Utilities</li>
		<li><a href="#"><i class="icon-user"></i> Profile</a></li>-->

	</ul>
</div>
