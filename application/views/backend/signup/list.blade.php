@layout('backend.layout')

@section('content')


<div class="span12">
    <h1>Signups List</h1>
    
    <ul class="nav nav-pills">
        
        <li class="{{ URI::segment(3) == false ? 'active' : ''}}">{{ HTML::link('admin/signup', 'All') }}</li>
        
        @foreach (League::all() as $league)
        <li class="{{ URI::segment(4) == $league->id ? 'active' : ''}}">{{ HTML::link('admin/signup/filter/' . $league->id, $league->name) }}</li>
        @endforeach
    </ul>
    
    @if (Session::has('message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Success!</strong> {{ Session::get('message') }}
    </div>
    @endif
    
    <table class="table table-hover table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>League Name</th>
                <th>Team Name</th>
                <th>Bar Name</th>
                <th>Created At</th>
                <th>Registrants</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 1; ?>
            @foreach ($signups as $signup)
            <tr>
                <td>{{ $i }}</td>
                <td>{{ ($signup->league_id > 0) ? $signup->league->name : ''}}</td>
                <td>{{ $signup->team_name }}</td>
                <td>{{ $signup->bar_name }}</td>
                <td>{{ date('m-d-Y H:i', strtotime($signup->created_at)) }}</td>
                <td>
                    <?php $registrants = DB::table('registrants')->where('signup_id', '=', $signup->id)->get(); ?>
                    @foreach ($registrants as $registrant)
                    {{ $registrant->fullname }}<br/>
                    @endforeach
                </td>
                <td>{{ HTML::link("admin/signup/" . $signup->id, 'View', array('class' => 'btn btn-info')) }}</td>
            </tr>
            
            <?php $i++; ?>
            @endforeach
        </tbody>
    </table>
</div>



@endsection