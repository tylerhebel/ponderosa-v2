@layout('backend.layout')

@section('content')

<style type="text/css">
    @media print {
        * { margin: 0 !important; padding: 0 !important; }
        #controls, .footer, .footerarea{ display: none; }
        html, body {
          /*changing width to 100% causes huge overflow and wrap*/
          height:100%; 
          overflow: hidden;
          background: #FFF; 
          font-size: 9.5pt;
        }

        .template { width: auto; left:0; top:0; }
        img { width:100%; }
        li { margin: 0 0 10px 20px !important;}
        .navbar {display: none;}
        ul.nav {display: none;}
      }
</style>

<div class="page-header">
    <h1>Signup</h1>
</div>

<div class="row-fluid">
    <div class="span4">
        <h4>League Name</h4>
        <p>{{ $signup->league->name }}</p>

        <h4>Team Name</h4>
        <p>{{ $signup->team_name }}</p>

        <h4>Bar Name</h4>
        <p>{{ $signup->bar_name }}</p>
    </div>

    <div class="span8">
        <?php $i = 0; ?>
        @foreach ($signup->registrants as $player)
        <h3>{{ ($i+1) }} - {{ $player->fullname }}{{ $i == 0 ? '<i> - Team Captain</i>' : '' }}</h3>
            <strong>Address</strong>
        <p>{{ $player->address }}</p>
            <strong>Phone</strong>
        <p>{{ $player->phone }}</p>
            <strong>Email</strong>
        <p>{{ $player->email }}</p>
        <?php $i++; ?>
        @endforeach
    </div>

</div>

<div class="form-actions">
    {{ HTML::link('#', 'Print', array('class' => 'btn btn-primary', 'id' => 'print_signup')) }}
    {{ HTML::link('admin/signup', 'Go Back', array('class' => 'btn')) }}
    {{ HTML::link("admin/signup/delete/$signup->id", 'Delete', array('id' => 'delete_signup', 'class' => 'btn btn-danger pull-right')) }}
</div>
    
@endsection