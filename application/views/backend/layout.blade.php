
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Admin Panel</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    {{ HTML::style('backend/css/bootstrap.min.css') }}
    {{ HTML::style('backend/css/bootstrap-wysihtml5-0.0.2.css') }}
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
    </style>
    {{ HTML::style('backend/css/bootstrap-responsive.min.css') }}

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>

  <body>

<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container-fluid">
			<a class="btn btn-navbar" data-toggle="collapse"
				data-target=".nav-collapse"> <span class="icon-bar"></span> <span
				class="icon-bar"></span> <span class="icon-bar"></span>
			</a> 
			<a class="brand" href="{{ URL::to('admin') }}">Ponderosa Admin Panel</a>
			<div class="nav-collapse">
				<ul class="nav">
					<li class="{{ URI::segment(2) == '' ? 'active' : ''}}"><a href=" {{ URL::to('admin') }}">Dashboard</a></li>
					<li class="{{ URI::segment(2) == 'league' ? 'active' : ''}}"><a href="{{ URL::to('admin/league') }}">Leagues</a></li>
					<li class="{{ URI::segment(2) == 'season' ? 'active' : ''}}"><a href="{{ URL::to('admin/season') }}">Seasons</a></li>
					<!--
					<li class="{{ URI::segment(2) == 'standings' ? 'active' : ''}}"><a href="{{ URL::to('admin/standings') }}">Standings</a></li>
					<li class="{{ URI::segment(2) == 'schedule' ? 'active' : ''}}"><a href="{{ URL::to('admin/schedule') }}">Schedules</a></li>
					-->
					<li class="{{ URI::segment(2) == 'user' ? 'active' : ''}}"><a href="{{ URL::to('admin/user') }}">Users</a></li>
                    <li class="{{ URI::segment(2) == 'signup' ? 'active' : ''}}"><a href="{{ URL::to('admin/signup') }}">Signups</a></li>
                    <li class="{{ URI::segment(2) == 'content' ? 'active' : ''}}"><a href="{{ URL::to('admin/content') }}">Content</a></li>
                    <li class="{{ URI::segment(2) == 'download' ? 'active' : ''}}"><a href="{{ URL::to('admin/download') }}">Download</a></li>
				</ul>
				
				<ul class="nav pull-right">
                    
					<li class="dropdown">
    					<a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->firstname }} <b class="caret"></b></a>
    					<ul class="dropdown-menu">
                            <li>{{ HTML::link('/', 'Go to Frontend') }}</li>
      						<li>{{ HTML::link('logout', 'Logout') }}</li>
    					</ul>
  					</li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>
</div>


    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span3">
                @include('backend.nav')
            </div>
            <div class="span9">
                @yield('content')
            </div>
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; Company 2012</p>
      </footer>

    </div><!--/.fluid-container-->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    {{ HTML::script('js/vendor/jquery-1.12.4.min.js') }}
    {{ HTML::script('backend/js/bootstrap.js') }}
    {{ HTML::script('backend/js/wysihtml5-0.3.0.js') }}
    {{ HTML::script('backend/js/bootstrap-wysihtml5-0.0.2.min.js') }}
    {{ HTML::script('backend/js/scripts.js') }}
  </body>
</html>
