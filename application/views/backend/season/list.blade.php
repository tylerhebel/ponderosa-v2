@layout('backend.layout')

@section('content')

<div class="span12">
    <h1>Season List</h1>
    
    @if (Session::has('message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Success!</strong> {{ Session::get('message') }}
    </div>
    @endif
    
    {{ HTML::link('admin/season/create', 'Create New Season', array('class' => 'btn')) }}
    <br/><br/>
    <table class="table table-hover table-bordered table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Active</th>
                <th>Registration Open</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($seasons as $season)
            <tr>
                <td>{{ $season->name }}</td>
                <td>{{ Yesno::write($season->active) }}</td>
                <td>{{ Yesno::write($season->registration_open) }}</td>
                <td>{{ HTML::link("admin/season/" . $season->id, 'Edit', array('class' => 'btn btn-warning')) }}</td>
                
            </tr>
            @endforeach
        </tbody>
    </table>
</div>



@endsection