@layout('backend.layout')

@section('content')

<?php

$create = isset($season) ? false : true; 

?>

<div class="span12">
    @if ($create)
    <h1>Create New Season</h1>
    @else
    <h1>Edit Season <small>{{ $season->name }}</small></h1>
    @endif
    
    {{ Form::open('admin/season/' . ($create ? 'create' : $season->id), 'POST', array('class' => 'form-horizontal well')) }}

    <div class="control-group <?php echo ($errors->has('name')) ? 'error' : ''; ?>">
        {{ Form::label('name', 'Season Name', array('class' => 'control-label')) }}

        <div class="controls">
            {{ Form::text('name', Input::old('name') || $create ? Input::old('name') : $season->name, array('class' => 'input-xlarge')) }}
            @if ($errors->has('name'))
            <span class="help-inline">{{ $errors->first('name') }}</span>
            @endif
        </div>
    </div>
    <div class="control-group">
        {{ Form::label('active', 'Is Active?', array('class' => 'control-label')) }}

        <div class="controls">
            <label class="checkbox">{{ Form::checkbox('active', 'active', isset($season->active) && $season->active == 1 ? 1 : 0) }}</label>
            <span class="help-block">"Is Active" controls if leagues belonging to this season appear on the frontend</span>
        </div>
    </div>
    <div class="control-group">
        {{ Form::label('active', 'Registration Open?', array('class' => 'control-label')) }}

        <div class="controls">
            <label class="checkbox">{{ Form::checkbox('registration_open', 'registration_open', isset($season->registration_open) && $season->registration_open == 1 ? 1 : 0) }}</label>
            <span class="help-block">"Registration Open" controls if players can sign up for this league on the frontend</span>
        </div>
    </div>
    <div class="form-actions">
        {{ Form::submit('Save', array('class' => 'btn btn-success')) }}
        {{ HTML::link('admin/season', 'Cancel & Go Back', array('class' => 'btn')) }}
        
        @if (!$create)
        {{ HTML::link("admin/season/delete/$season->id", 'Delete', array('id' => 'delete_season', 'class' => 'btn btn-danger pull-right')) }}
        @endif
    </div>
    {{ Form::close() }}
</div>

@endsection