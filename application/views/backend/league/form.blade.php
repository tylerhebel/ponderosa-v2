@layout('backend.layout')

@section('content')

<?php

$create = isset($league) ? false : true; 

?>

<div class="span12 league_admin_page">
    @if ($create)
    <h1>Create New League</h1>
    @else
    <h1>Edit League <small>{{ $league->name }}</small></h1>
    @endif
    
    @if (Session::has('message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Success!</strong> {{ Session::get('message') }}
    </div>
    @endif
    
    {{ Form::open('admin/league/' . ($create ? 'create' : $league->id), 'POST', array('class' => 'form-horizontal well')) }}

    <div class="tabbable"> <!-- Only required for left/right tabs -->
      <ul class="nav nav-pills">
        <li class="active"><a href="#league" data-toggle="tab">League</a></li>
        <li><a href="#standings" data-toggle="tab">Standings</a></li>
        <li><a href="#schedule" data-toggle="tab">Schedule</a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="league">
          <div class="control-group <?php echo ($errors->has('name')) ? 'error' : ''; ?>">
            {{ Form::label('name', 'League Name', array('class' => 'control-label')) }}

                <div class="controls">
                    {{ Form::text('name', Input::old('name') || $create ? Input::old('name') : $league->name, array('class' => 'input-xlarge')) }}
                    @if ($errors->has('name'))
                    <span class="help-inline">{{ $errors->first('name') }}</span>
                    @endif
                    <p class="help-block">
                        This needs to match the LeagueLeader league name to automatically pull standings and schedules.
                        <br/><a href="#">View List</a> (from LeagueLeader)
                    </p>
                </div>
            </div>

            <div class="control-group <?php echo ($errors->has('description')) ? 'error' : ''; ?>">
                {{ Form::label('description', 'League Description', array('class' => 'control-label')) }}

                <div class="controls">
                    {{ Form::textarea('description', Input::old('description') || $create ? Input::old('description') : $league->description, array('class' => 'input-xlarge')) }}
                    @if ($errors->has('description'))
                    <span class="help-inline">{{ $errors->first('description') }}</span>
                    @endif
                </div>
            </div>
            
            <div class="control-group <?php echo ($errors->has('captains')) ? 'error' : ''; ?>">
                {{ Form::label('captains', 'Team Captains Text', array('class' => 'control-label')) }}

                <div class="controls">
                    {{ Form::textarea('captains', Input::old('captains') || $create ? Input::old('captains') : $league->captains_blob, array('class' => 'input-xlarge')) }}
                    @if ($errors->has('captains'))
                    <span class="help-inline">{{ $errors->first('captains') }}</span>
                    @endif
                    <p class="help-block">
                        Copy and paste from LeagueLeader schedule report.
                    </p>
                </div>
            </div>
            
            <div class="control-group <?php echo ($errors->has('players_per_team')) ? 'error' : ''; ?>">
                {{ Form::label('players_per_team', 'Players per Team', array('class' => 'control-label')) }}

                <div class="controls">
                    {{ Form::select('players_per_team', League::getPlayersPerTeamSizes(), Input::old('players_per_team') || $create ? Input::old('players_per_team') : $league->players_per_team) }}
                    @if ($errors->has('players_per_team'))
                    <span class="help-inline">{{ $errors->first('players_per_team') }}</span>
                    @endif
                </div>
            </div>

            <div class="control-group">
                {{ Form::label('active', 'Is Active?', array('class' => 'control-label')) }}

                <div class="controls">
                    <label class="checkbox">{{ Form::checkbox('active', 'active', isset($league->active) && $league->active == 1) }}</label>
                </div>
            </div>

            <div class="control-group <?php echo ($errors->has('season_id')) ? 'error' : ''; ?>">
                {{ Form::label('season_id', 'Season', array('class' => 'control-label')) }}

                <div class="controls">
                    {{ Form::select('season_id', $seasons, Input::old('season_id') || $create ? Input::old('season_id') : $league->season_id) }}
                    @if ($errors->has('season_id'))
                    <span class="help-inline">{{ $errors->first('season_id') }}</span>
                    @endif
                </div>
            </div>


        </div>


        <div class="tab-pane" id="standings">
            <div class="control-group">
                {{ Form::label('standings-content', 'Standings File', array('class' => 'control-label')) }}

                <div class="controls">
                    {{ Form::textarea('standings-content', Input::old('standings-content') || $create ? Input::old('standings-content') : $league->standing->content, array('class' => 'span-fluid', 'style' => 'font-family:monospace;')) }}
                    <p class="help-block">
                        <button class="btn" id="fetch-standings"><i class="icon-refresh"></i> Refresh</button>
                        <button class="btn" id="clear-standings"><i class="icon-remove-circle"></i> Clear</button>
                    </p>
                    <hr/>
                    
                    @if ( ! $create)
                    <?php
                    $now = new DateTime();
                    $then = new DateTime($league->standing->updated_at);
                    $diff = $now->diff($then);
                    ?>
                    <p><i>Last updated</i>: {{ $diff->format('<b>%a</b> days ago') }}</p>
                    @endif
                </div>
            </div>
        </div>


        <div class="tab-pane" id="schedule">
          <div class="control-group">
                {{ Form::label('schedule-content', 'Schedule File', array('class' => 'control-label')) }}

                <div class="controls">
                    {{ Form::textarea('schedule-content', Input::old('schedule-content') || $create ? Input::old('schedule-content') : $league->schedule->content, array('class' => 'span-fluid', 'style' => 'font-family:monospace;')) }}
                    <p class="help-block">
                        <button class="btn" id="fetch-schedule"><i class="icon-refresh"></i> Refresh</button>
                        <button class="btn" id="clear-schedule"><i class="icon-remove-circle"></i> Clear</button>
                        <hr/>
                        @if ( ! $create)
                        <?php
                        $now = new DateTime();
                        $then = new DateTime($league->schedule->updated_at);
                        $diff = $now->diff($then);
                        ?>
                        <p><i>Last updated</i>: {{ $diff->format('<b>%a</b> days ago') }}</p>
                        @endif
                    </p>
                </div>
            </div>
        </div>
      </div>

      <div class="form-actions">
        {{ Form::submit('Save changes', array('class' => 'btn btn-success')) }}
        {{ HTML::link('admin/league', 'Cancel & Go Back', array('class' => 'btn')) }}
        
        @if (!$create)
        {{ HTML::link("admin/league/delete/$league->id", 'Delete', array('id' => 'delete_league', 'class' => 'btn btn-danger pull-right')) }}
        @endif
    </div>
    </div>

    <input type="hidden" name="standings_url" id="standings_url" value="{{ URL::to('api/standings/') }}" />
    <input type="hidden" name="schedule_url" id="schedule_url" value="{{ URL::to('api/schedule/') }}" />

    {{ Form::close() }}
</div>

@endsection