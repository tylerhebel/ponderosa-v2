@layout('backend.layout')

@section('content')

<div class="span12">
    <h1>League List</h1>
        
    @if (Session::has('message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Success!</strong> {{ Session::get('message') }}
    </div>
    @endif
    
    {{ HTML::link('admin/league/create', 'Create New League', array('class' => 'btn')) }}
    <br/><br/>
    <table class="table table-hover table-bordered table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Players per Team</th>
                <th>Active</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($leagues as $league)
            <tr>
                <td>{{ $league->name }}</td>
                <td>{{ $league->description }}</td>
                <td>{{ $league->players_per_team }}</td>
                <td>{{ Yesno::write($league->active) }}</td>
                <td>{{ HTML::link("admin/league/$league->id", 'Edit', array('class' => 'btn btn-warning')) }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>



@endsection