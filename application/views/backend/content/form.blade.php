@layout('backend.layout')

@section('content')

<?php

$create = isset($content) ? false : true; 

?>

<div class="span12" id="content_page">
    @if ($create)
    <h1>Create New Content</h1>
    @else
    <h1>Edit Content</h1>
    @endif
    
    {{ Form::open('admin/content/' . ($create ? 'create' : $content->id), 'POST', array('class' => 'form-horizontal well')) }}

    <div class="control-group <?php echo ($errors->has('name')) ? 'error' : ''; ?>">
        {{ Form::label('title', 'Content Title', array('class' => 'control-label')) }}

        <div class="controls">
            {{ Form::text('title', Input::old('title') || $create ? Input::old('title') : $content->title, array('class' => 'input-xlarge')) }}
            @if ($errors->has('title'))
            <span class="help-inline">{{ $errors->first('title') }}</span>
            @endif
        </div>
    </div>
    <div class="control-group">
        {{ Form::label('active', 'Is Active?', array('class' => 'control-label')) }}

        <div class="controls">
            <label class="checkbox">{{ Form::checkbox('active', 'active', isset($content->active) && $content->active == 1 ? 1 : 0) }}</label>
            <span class="help-block">"Is Active" controls if this content appears on frontend.</span>
        </div>
    </div>
    <div class="control-group">
        {{ Form::label('body', 'Body', array('class' => 'control-label')) }}

        <div class="controls">
            {{ Form::textarea('body', Input::old('body') || $create ? Input::old('body') : $content->body, array('class' => 'span-fluid', 'style' => 'font-family:monospace;', 'rows' => 20)) }}
            <span class="help-block"></span>
        </div>
    </div>
    <div class="form-actions">
        {{ Form::submit('Save', array('class' => 'btn btn-success')) }}
        {{ HTML::link('admin/content', 'Cancel & Go Back', array('class' => 'btn')) }}
        
        @if (!$create)
        {{ HTML::link("admin/content/delete/$content->id", 'Delete', array('id' => 'delete_content', 'class' => 'btn btn-danger pull-right')) }}
        @endif
    </div>
    {{ Form::close() }}
</div>

@endsection