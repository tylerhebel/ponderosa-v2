@layout('backend.layout')

@section('content')

<div class="span12">
    <h1>Content</h1>
        
    @if (Session::has('message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Success!</strong> {{ Session::get('message') }}
    </div>
    @endif
    
    {{ HTML::link('admin/content/create', 'Create New Content', array('class' => 'btn')) }}
    <br/><br/>
    <table class="table table-hover table-bordered table-striped">
        <thead>
            <tr>
                <th>Title</th>
                <th>Active</th>
                <th>Author</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($contents as $content)
            <tr>
                <td>{{ $content->title }}</td>
                <td>{{ Yesno::write($content->active) }}</td>
                <td>{{ User::find($content->user_id)->firstname }}</td>
                <td>{{ HTML::link("admin/content/$content->id", 'Edit', array('class' => 'btn btn-warning')) }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>



@endsection