<header>
    <div id="header">
        <div class="row">
            <div class="nine columns promo">
                <h1>Ponderosa Amusements</h1>
                <p>Featuring leagues for all skill levels, playing year round. Online league signup makes it easy for players to get started.</p>
                <a class="large button radius" id="signup_button" href="{{ URL::to('signup') }}">Signup Now</a>
            </div>
            <div class="three columns">
                <?php echo HTML::image('img/logo_black.jpg'); ?>
            </div>
        </div>
    </div>
</header>