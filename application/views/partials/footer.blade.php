<footer>
    <div class="row">
        <div class="twelve columns">
            <div class="row">
                <div class="four columns">
                    <h3>Contact Us</h3>
                    <h5>Ponderosa Amusements</h5>
                    <address>
                        1424 Pando Avenue<br>
                        Colorado Springs, CO 80905<br>
                    </address>	
                </div>
                <div class="four columns">
                    <h3>League Contact</h3>
                    <dl>
                        <dt>Lisa Carazo</dt>
                        <dd>ponderosadarts@yahoo.com</dd>
                        <dd>719-650-7753</dd><br/>
                        <dt>Todd Torvik</dt>
                        <dd>ponderosafun@aol.com</dd>
                    </dl>
                </div>
                <div class="four columns">
                    <h3>Downloads</h3>
                    <?php $downloads = Download::all(); ?>
                    
                    <ul class="disc">
                    @foreach ($downloads as $download)
                    <li>
                        {{ HTML::link("download/$download->id", $download->title) }}
                    </li>
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>


<script>
    var _gaq=[['_setAccount','UA-29485147-1'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
</script>

<!-- End Footer -->