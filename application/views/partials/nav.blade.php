<!-- Navigation -->

<div class="contain-to-grid fixed">
    <nav class="top-bar fixed">
        <ul>
            <li class="name"><h1><a href="{{ URL::to('') }}">Ponderosa Amusements</a></h1></li>
            <li class="toggle-topbar"><a href="#"></a></li>
        </ul>

        <section>
            <ul class="right">
                
                <li>{{ HTML::link('signup', 'Online Signup') }}</li>
            @if (Auth::check())
                <li class="has-dropdown">
                    
                    {{ HTML::link('user/profile', "Welcome, " . Auth::user()->firstname) }}
                    <ul class="dropdown">
                        <li>{{ HTML::link('user/profile', "My Account") }}</li>
                        @if (Auth::user()->is_admin == 1)
                        <li>{{ HTML::link('admin', 'Admin Panel') }}</li>
                        @endif
                        <li>{{ HTML::link('logout', 'Logout') }}</li>
                    </ul>
                </li>
            @else
                <li>{{ HTML::link('login', 'Login') }}</li>
            @endif
                
            </ul>

        </section>
    </nav>
</div>
<!--[if lt IE 9]>
<div class="upgradebar">
<div class="alert-box secondary">
<div class="row">
<div class="twelve columns">
    You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.
</div>
</div>
</div>
</div>
<![endif]-->
<!-- End Navigation -->