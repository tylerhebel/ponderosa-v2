<?php

class Yesno
{
    public static function write($value)
    {
        if ($value == 1)
        {
            return "Yes";
        }
        else if ($value == 0)
        {
            return "No";
        }
        
        return $value;
    }
}

?>